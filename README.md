# k8s-system-tst

System configuration of test cluster at ID-SD. Uses the k8s-system-base
repository as a base layer for kustomize.

## Directoy Structure

There is a subdirectory for each cluster. In this directory, a directory
for every component exist, similar to the base directory. Every component
directory must contain a kustomization.yml file.

The subdirectory `argocd` contains the same subdirectory structure as the
root, one directory per cluster. These directories contain ArgoCD objects.
An "apps of apps" application in the k8s-argocd repository accesses these
directories, read the ArgoCD objects, and ArgoCD deploys the components
to the cluster.

```
-+- argocd -+- cert-manager.yml
 |          |
 |          +- ingress-nginx.yml
 |
 +- cert-manager  --- kustomization.yml, ingress.yml, config.yml etc.
 |
 +- ingress-nginx --- kustomization.yml, ingress.yml, config.yml etc.
```

